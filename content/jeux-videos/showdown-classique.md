# Pokémon Showdown classique

Le club jeux-video organise un tournoi de pokémon showdown random battle le
dimanche à 19h.

Vous connaissez certainement Pokémon, et il est probable que vous ayez essayé ce
jeu au moins une fois, mais avez-vous déjà tenté d’y faire des combats
stratégiques ? C’est le but derrière Showdown, un simulateur en ligne qui vous
permet de faire des combats contre d’autres joueurs sans même avoir besoin de
posséder une cartouche de jeu. Beaucoup de choses y sont possibles, des matchs
avec des pokémons générées aléatoirement jusqu’à la créations d’équipes où vous
pouvez gérer toutes les caractéristiques de vos créatures.

Le tournoi suivra les règles de « l’Overused ». Si vous ne connaissez pas il
vous suffit d’aller voir la catégorie « Play Restrictions » dans ce lien pour
voir ce qui n’est pas autorisé : https://www.smogon.com/dex/ss/formats/ou/ .
Retenez qu’en gros presque tout y est permis mis à part les gros pokémons
légendaires comme Rayquaza, Giratina et companie, ainsi que certaines techniques
anti-jeu.  Pour ce format il est nécessaire d’avoir ses propres teams. Si vous
voulez la créer vous même il vous suffira d’aller sur Showdown dans
« Teambuilder », puis dans « New Team », de sélectionnez le format « OU », et ensuite
de choisir vos pokémons et leurs caractéristiques. Si vous n’avez pas envie de
vous prendre la tête avec ça avant le tournoi on aura plusieurs équipes à vous
prêter juste avant, et on vous expliquera comment les récupérer facilement.



