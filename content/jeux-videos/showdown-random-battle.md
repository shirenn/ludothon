# Pokémon Showdown random battle

Le club jeux-video organise un tournoi de pokémon showdown random battle le
lundi à 19h.

Vous connaissez certainement Pokémon, et il est probable que vous ayez essayé ce
jeu au moins une fois, mais avez-vous déjà tenté d’y faire des combats
stratégiques ? C’est le but derrière Showdown, un simulateur en ligne qui vous
permet de faire des combats contre d’autres joueurs sans même avoir besoin de
posséder une cartouche de jeu. Beaucoup de choses y sont possibles, des matchs
avec des pokémons générées aléatoirement jusqu’à la créations d’équipes où vous
pouvez gérer toutes les caractéristiques de vos créatures.

Le tournoi sera en random battle. Pour y jouer rien de plus simple. Vous allez
sur https://play.pokemonshowdown.com/ et vous cliquez directement sur BATTLE.
Vous serez alors automatiquement lancés dans un combat avec 6 pokémons choisis
aléatoirement, contre un adversaire ayant lui aussi une équipe aléatoire.
L’interface permet de voir les types des pokémons, leurs statistiques, les
effets des attaques, et il existe même des commandes permettant de voir les
faiblesses des pokémons ! Il suffit simplement de savoir lire un peu l’anglais.
Donc même si vous avez lâché la série depuis un bout de temps n’hésitez pas à
venir pour vous amuser.

Si vous avez la moindre question sur l’organisation, sur les règles ou sur
Showdown en général, n’hésitez pas à contacter Meracx#9687 sur Discord ou par
mail : lucas.cottrel@laposte.net

