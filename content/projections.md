---
title: Projections
menu:
  main:
    weight: 3
---

# Projections

## Ciné-club

  - [Le parrain](cine-club/le-parrain), le lundi à 21h
  - [Lost in translation](cine-club/lost-in-translation), le
    vendredi à 21h

## Animens

  - [Les enfants du temps](animens/les-enfants-du-temps), le mardi à
    21h
  - [Loin de moi, près de toi](animens/loin-de-moi-pres-de-toi), le
    dimanche à 21h

## Nanarens

  - [Turbulences 3](nanarens/turbulences-3), le mercredi à 21h
  - [Fusion core](nanarens/fusion-core), le samedi à 21h
