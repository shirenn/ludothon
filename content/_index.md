---
title: Accueil
menu:
  main:
    weight: 1
---

Le Ludothon est une semaine d'activités ludique organisé par le BDL. Tu y
trouveras entre autres, des jeux de sociétés, des jeux de rôles, des projections
de films, des jeux d'ambiances, des jeux videos …

# Planning
{{< figure src="/images/planning.png" title="Planning du Ludothon" >}}

# Activités

## Jeux de sociétés

Pendant toute la durée du Ludothon, viens jouer avec nous à une large séléction
de jeux de sociétés. Rendez vous sur les channels du discord prévu à cet effet.

## Jeux d'ambiances

  - [Niche écologique](ambiance/niche-ecologique), le vendredi à 18h
  - [Chaine alimentaire](ambiance/chaine-alimentaire), le samedi à 10h
  - [Diplomacy](ambiance/diplomacy), le samedi à 20h
  - [Vaches aux enchères](ambiance/vaches-aux-encheres), le dimanche à 13h

## Projections

### Ciné-club

  - [Le parrain](projections/cine-club/le-parrain), le lundi à 21h
  - [Lost in translation](projections/cine-club/lost-in-translation), le
    vendredi à 21h

### Animens

  - [Les enfants du temps](projections/animens/les-enfants-du-temps), le mardi à
    21h
  - [Loin de moi, près de toi](projections/animens/loin-de-moi-pres-de-toi), le
    dimanche à 21h

### Nanarens

  - [Turbulences 3](projections/nanarens/turbulences-3), le mercredi à 21h
  - [Fusion core](projections/nanarens/fusion-core), le samedi à 21h


## Jeux de rôles

  - [One shot : *Chroniques oubliées*](jdr/one-shot-chroniques-oubliees), le vendredi à 18h
  - [One shot : *L'énigme du coffre*](jdr/one-shot-enigme-du-coffre), le samedi
    à 14h

## Jeux vidéos

  - [Tournoi de pokémon showdown random battle](jeux-videos/showdown-random-battle), le lundi à 19h
  - [Tournoi de pokémon showdown classique](jeux-videos/showdown-classique), le
    dimanche à 19h
  - [Tournoi smash](jeux-videos/smash), le mercredi à 19h

## Initiations

  - [Bridge](initiations/bridge), le dimanche à 10h
  - [Mahjong](initiations/mahjong), le lundi à 19h et le jeudi à 18h
  - [Magic](initiations/magic), le mardi à 18h
    - [Draft magic](initiations/magic/draft), le samedi à 14h
  - [Atelier d'écriture](initiations/atelier-d-ecriture), le mardi à 18h

