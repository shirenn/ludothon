# Fusion core

Projection organisée par le club-nanar le samedi à partir de 21h.

Pour des raisons inconnues, le noyau interne de la Terre a cessé de tourner sur
lui-même. Le champ magnétique de la planète s'effondre, provoquant sous les
latitudes les plus diverses une dramatique et spectaculaire série d'accident.
Face à cette catastrophe sans précédent, le gouvernement américain convoque le
géophysicien Josh Keyes, une poignée de savants de réputation internationale et
un tandem de spationautes, la Major Rebecca Childs et le Commandant Iverson.
Leur objectif : lancer dans les profondeurs du manteau terrestre une capsule
habitée et provoquer des explosions nucléaires en chaîne pour réactiver le noyau
et ainsi sauver le monde d'une destruction imminente


