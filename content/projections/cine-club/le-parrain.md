# Le parrain

Projection organisée par le ciné-club le lundi à partir de 21h.

## Synopsis

New York, 1945. Don Vito Corleone règne sur l'une des familles les plus
puissantes de la mafia italo-américaine. Virgil Sollozzo, qui dirige la famille
Tattaglia, lui propose de s'associer sur un nouveau marché prometteur : le
trafic de drogue, mais celui-ci refuse. Don Vito est alors pris pour cible lors
d'une tentative d'assassinat en pleine rue. Criblé de balles, il survit par
miracle. Son fils Michael, qui s'était toujours tenu à l'écart des affaires
familiales, se porte volontaire pour le venger.
