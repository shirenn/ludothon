# Lost in translation

Projection organisée par le ciné-club le vendredi à partir de 21h.

## Synopsis

Bob Harris, star de cinéma sur le déclin, arrive à Tokyo pour tourner une
publicité pour une marque de whisky. Dans l'hôtel de luxe où il est installé,
victime du décalage horaire, il ne sait que faire pour occuper ses nuits. Dans
ce même hôtel, Charlotte, une jeune Américaine tout juste sortie de
l'université, accompagne son mari, photographe de mode de la jet-set. Ce dernier
semble s'intéresser davantage à son travail qu'à sa femme. Un soir, Bob et
Charlotte font connaissance au bar.

