# Tenki no Ko, Les enfants du temps

Projection organisée par animens le mardi à partir de 21h.

Le jeune lycéen Hodaka Morishima quitte son domicile sur une île isolée pour
s'installer à Tokyo, mais il manque rapidement d'argent. Il vit dans l'isolement
mais trouve finalement un travail en tant qu'écrivain pour un magazine occulte
louche. Après que Hodaka commence à travailler, le temps reste pluvieux jour
après jour. Puis, dans un coin très fréquenté de la ville, il rencontre une
jeune fille nommée Hina Amano. Hina et son jeune frère vivent ensemble mais
mènent une vie joyeuse et stable. Cette fille enjouée et déterminée possède un
pouvoir étrange et merveilleux : le pouvoir d'arrêter la pluie et de dégager le
ciel.

