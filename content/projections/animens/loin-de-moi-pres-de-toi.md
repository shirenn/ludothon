# Nakitai Watashi wa Neko wo Kaburu, Loin de moi, près de toi

Projection organisée par animens le dimanche à partir de 21h.

Une collégienne se transforme en chat pour attirer l'attention d'un garçon, sans
se rendre compte que la frontière entre l'humain et l'animal s'estompe peu à
peu.
