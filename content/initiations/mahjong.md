# Initiations mahjong

Sans doute avez-vous déjà aperçu ces élégantes tuiles énigmatiques. Peut-être
avez-vous même déjà ouï les calmes mais péremptoires exclamations : *Pon !*,
*Riichi !* ou même *Ron !*. Venez découvrir le mahjong et ses multiples figures
! Se jouant à trois ou à quatre joueurs pour des parties plus ou moins longues,
nous serons heureux de vous présenter toutes les possibilités qu'offre ce jeu.
La séance sera une introduction suivie de quelques parties. Si cela vous plaît,
il sera possible de continuer sur le discord de la med ou en présentiel dès la
fin du confinement et si vous avez des questions vous pouvez contacter
pétaire#4245, Nicomarg#4279, Sarcozy#6863 ou même le rôle Mahjong sur le
\#mahjong du discord de la med, nous serons heureux de vous répondre (et de
jouer).

