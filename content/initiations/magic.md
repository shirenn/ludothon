# Initiations Magic The Gathering

Pour ceux qui n'ont jamais vu des tables couvertes de cartes avec des joueurs en
train de reprendre dix fois des calculs visiblement très compliqués, qu'est-ce
que Magic : the Gathering ?

C'est un jeu de cartes à jouer et à collectioner, dans lequels vous incarnez un
puissant magicien arpentant les divers plans du multivers, manipulant de
puissantes créatures et sorts afin de l'emporter sur vos adversaires.
(Relativement) simple à apprendre mais difficile à maîtriser, Magic : the
Gathering brille par sa profondeur stratégique, la grande diversité apportée par
ses dizaines de miliers de cartes et sa constante évolution.

Pendant cette initiation on vous expliquera les règles du jeu, puis vous jouerez
quelques parties. Après cela, nhésitez pas à aller sur le discord du club
Magic : https://discord.gg/KYsFADe ou à contacter @Nicomarg#4279 sur discord.

