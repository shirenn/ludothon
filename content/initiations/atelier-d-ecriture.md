# Atelier d'Écriture

Le mardi à 18h, l'Atelier d'Écriture organisera une de ses séances dans le cadre
du Ludothon. Mais qu'est ce que l'Atelier d'Écriture me demanderas tu jeune
plume ? C'est un rendez-vous hebdomadaire, qui regroupe des étudiants de toutes
les promotions et de toutes les sections autour du plaisir d'écrire.  Le format
est le suivant : on donne un thème (exemple sur les thèmes précédents : L'ombre
d'un doute, « Comme une pierre tombant dans l'abîme du ciel » , …) et on écrit
pendant une heure dessus. Ça prend la forme qu'on veut, ça peut-être un poème,
une histoire ou n'importe quoi qui vous amuse. Au bout d'une heure, ceux qui le
veulent lisent leur texte. N'hésitez pas à vous joindre à nous !
