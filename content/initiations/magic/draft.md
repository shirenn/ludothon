# Magic the Gathering − Draft

Qu'est-ce qu'un Draft Magic : the Gathering ? Il s'agit d'un événement/tournoi
de « limité », ce qui ceut dire qu'on va prendre des paquets (virtuels,
coronavirus oblige) contenant des cartes aléatoires d'une extension, les ouvrir
et jouer avec ce qu'on va ouvrir dedans.

La subtilité supplémentaire du draft est qu'on ne va pas chacun ouvrir nos
paquets dans notre coin tous seuls. Pour chaque paquet, chaque joueur va ouvrir
son paquet, regarder les 15 cartes dedans, en choisir une puis passer le paquet
à son voisin (et donc recevoir un nouveau paquet de 14 cartes de son autre
voisin). On continue ainsi jusqu'à avoir épuisé toutes les cartes. Enfin, chacun
crée un deck avec les cartes qu'il a récupéré et on joue les uns contre les
autres.

Ce qui fait toute la saveur de ce format c'est cette phase de « draft » (quand
on prend nos cartes), où il faut à la fois bien choisir ses cartes pour former
un ensemble cohérent, mais aussi empêcher les autres joueurs de récupérer des
cartes trop puissantes.

Le set choisi sera soit Zendikar Rising soit Commander Legends en fonction des
préférences des personnes présentes

