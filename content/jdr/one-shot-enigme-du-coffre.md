# One shot − Énigme du coffre

Le club Jeux de Rôles et Murders organise une session de jeu de rôle oneshot le
samedi à partir de 14h.

« Plus d'or à Kaamelott ! Depuis deux mois, des chargements arrivant d'une mine
du nord ne sont jamais arrivés au château. Devant les récriminations incessantes
du père Blaise ("Il y a plus un radis dans la boutique, bon Dieu !"), Arthur et
ses fidèles chevaliers ne peuvent se résoudre qu'à un seul choix : aller
eux-mêmes enquêter et récupérer leur bien. "Et accessoirement leur faire passer
le goût des retards, à ces enfoirés" a conclut le seigneur Léodagan. »

