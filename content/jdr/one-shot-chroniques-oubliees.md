# One shot − Chroniques oubliées

Le club Jeux de Rôles et Murders organise une session de jeu de rôle oneshot le
vendredi à partir de 18h.

« Au cœur d'une forêt profonde où les personnages se sont perdus, un petit
village est paralysé par la peur que leur inspire une bête mystérieuse
responsable de plusieurs disparitions. Un simple animal féroce? Les habitants
soupçonnent qu'il y a une force plus sombre à œuvre. Les joueurs devront
rapidement élucider le mystère avant qu'un nouveau drame se produise … »



