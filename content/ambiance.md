---
title: Jeux d'ambiance
menu:
  main:
    weight: 2
---

# Jeux d'ambiance

  - [Niche écologique](niche-ecologique), le vendredi à 18h
  - [Chaine alimentaire](chaine-alimentaire), le samedi à 10h
  - [Diplomacy](diplomacy), le samedi à 20h
  - [Vaches aux enchères](vaches-aux-encheres), le dimanche à 13h
