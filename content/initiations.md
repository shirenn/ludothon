---
title: Initiations
menu:
  main:
    weight: 6
---

# Initiations

  - [Bridge](bridge), le dimanche à 10h
  - [Mahjong](mahjong), le lundi à 19h et le jeudi à 18h
  - [Magic](magic), le mardi à 18h
    - [Draft magic](magic/draft), le samedi à 14h
  - [Atelier d'écriture](atelier-d-ecriture), le mardi à 18h
