# Vaches aux enchères

Le dimanche à 13h.

Dans Les vaches aux enchères, des vaches vont être mises aux enchères. Le
problème, c'est que ce sont des mauvaises vaches. Du coup, vous n'en voulez pas.
La nullité de la vache est représentée par la valeur de son numéro : plus la
vache est nulle, plus son numéro est élevé. Le principe sera donc de miser pour
éviter de recuperer les vaches ! Mais vu que vous n’êtes pas sans cœur, toutes
les vaches devront partir chez quelqu'un... De plus, il y a certaines vaches qui
s'entraident pour devenir moins nulles : recuperez-les et finissez avec le
meilleur troupeau !

