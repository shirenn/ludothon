# Chaîne alimentaire

Le samedi à 10h

Incarnez un animal parmi 13 possibles dans une chaîne alimentaire, et luttez
pour remplir votre condition de victoire. Proie ou prédateur, caméléon luttant
pour la vie ou serpent venimeux, chacun a un pouvoir spécial lui permettant de
survivre... ou de dévorer les plus faibles que soi !

