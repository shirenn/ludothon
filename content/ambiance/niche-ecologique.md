# Niche écologique

Le vendredi à 18h

Joueurs : **5**

Pour survivre, chacun se doit de trouver sa niche écologique, mais la bataille
est rude. Cinq groupes animaux s'affrontent pour faire survivre le plus grand
nombre possible de leurs espèces, mais attention, il n'y aura pas assez de place
pour tout le monde.

Dans ce jeu à gameplay symétrique, chacun des cinq joueurs contrôlera l'un des
ces groupes animaux et devra, chaque tour, envoyer l'un de ses animaux dans l'un
des quatre habitats disponibles. Malheureusement, seuls les animaux se
retrouvant seuls dans leur habitat survivront, et tous les autres mourront.

*Trigger Warning* : Ce jeu ne se veut pas réaliste par rapport aux concepts
phylogénétiques ou écologiques du monde animal.

