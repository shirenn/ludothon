# Diplomacy

Le samedi à partir de 20h jusqu'à très très tard dans la nuit.

Jeu de guerre sur plateau, sorte de Risk sans dés, qui laisse la part belle aux
négociations entre joueurs. Créé dans les années 50 . Maintes fois réédité (4
éditions différentes à la Med). Interminable.  Incroyable. Inimitable.

  - 2-beaucoup de joueurs
  - 3-8 heures

A l’aube du XXe siècle, l’Europe se prépare pour un nouveau conflit. Alors que
les armées se déplacent lentement et que des batailles dantesques se succèdent,
les diplomates ne ménagent pas leurs efforts pour former et dissoudre leurs
alliances, et s’assurer du contrôle des territoires les plus importants pour
ravitailler leurs armées.

