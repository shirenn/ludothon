---
title: Jeux vidéos
menu:
  main:
    weight: 5
---

# Jeux vidéos

  - [Tournoi de pokémon showdown random battle](showdown-random-battle), le lundi à 19h
  - [Tournoi de pokémon showdown classique](showdown-classique), le
    dimanche à 19h
  - [Tournoi smash](smash), le mercredi à 19h
